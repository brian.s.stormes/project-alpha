from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm

# Create your views here.


@login_required
def list_projects(request):
    list = Project.objects.filter(owner=request.user)
    context = {"projects": list}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    item = get_object_or_404(Project, id=id)
    context = {"project_object": item}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            project.owner = request.user
            return redirect("list_projects")
    else:
        form = CreateProjectForm()

    context = {"form": form}

    return render(request, "projects/create.html", context)
